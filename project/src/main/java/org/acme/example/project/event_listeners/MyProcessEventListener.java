package org.acme.example.project.event_listeners;

import org.jbpm.workflow.instance.node.WorkItemNodeInstance;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.event.process.ProcessNodeLeftEvent;
import org.kie.api.event.process.ProcessNodeTriggeredEvent;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.process.ProcessVariableChangedEvent;
import org.kie.api.runtime.process.NodeInstance;
import org.kie.api.definition.process.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class MyProcessEventListener implements ProcessEventListener {
    
    private static Logger log = LoggerFactory.getLogger("WS Audit");

    @Override
    public void beforeProcessStarted(ProcessStartedEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterProcessStarted(ProcessStartedEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeProcessCompleted(ProcessCompletedEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterProcessCompleted(ProcessCompletedEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeNodeTriggered(ProcessNodeTriggeredEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterNodeTriggered(ProcessNodeTriggeredEvent event) {
        Node currentNode = event.getNodeInstance().getNode();
        if ("review".equals(currentNode.getName())) {
            System.out.println("Found a node with the name review!");
            //get the outgoing connections
            org.kie.api.definition.process.Connection reviewNodeCon = currentNode.getOutgoingConnections(org.jbpm.workflow.core.Node.CONNECTION_DEFAULT_TYPE).get(0);
	        //get outgoing connection from the XOR gateway that follows the review node
	        List<org.kie.api.definition.process.Connection> conns = reviewNodeCon.getTo()
		        .getOutgoingConnections(org.jbpm.workflow.core.Node.CONNECTION_DEFAULT_TYPE);
	        //get possible node names for each outgoing conn from the XOR gateway
	        for (org.kie.api.definition.process.Connection gateWayCon : conns) {
                String nodeAfterGateway = gateWayCon.getTo().getName();
	            System.out.println("Possible future node: "+ nodeAfterGateway);
	        }
	    }
    }

    @Override
    public void beforeNodeLeft(ProcessNodeLeftEvent event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void afterNodeLeft(ProcessNodeLeftEvent event) {
	//this simply logs out the node name as it is left
        NodeInstance node = event.getNodeInstance();
        if (node instanceof WorkItemNodeInstance) {
            //Prints the name of the work item node
            WorkItemNodeInstance wiNode = (WorkItemNodeInstance) node;
            String nodeName = wiNode.getWorkItem().getName();

            System.out.println("afterNodeLeft: " + nodeName);
            log.info("afterNodeLeft: " + nodeName);
            // if ("WebService".equals(wiNode.getWorkItem().getName())) {
            //     String wsInterface = (String) wiNode.getWorkItem().getParameter("Interface");
            //     String wsOperation = (String) wiNode.getWorkItem().getParameter("Operation");
            //     String wsUrl = (String) wiNode.getWorkItem().getParameter("Url");
            //     log.info("WS invoked - Url: {}, Interface: {}, Operation: {}", wsUrl, wsInterface, wsOperation);
            // }
        }
    }

    @Override
    public void beforeVariableChanged(ProcessVariableChangedEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterVariableChanged(ProcessVariableChangedEvent event) {
        // TODO Auto-generated method stub

    }

}
