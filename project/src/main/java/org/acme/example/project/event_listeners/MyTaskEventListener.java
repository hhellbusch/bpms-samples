package org.acme.example.project.event_listeners;

import org.kie.api.task.TaskEvent;
import org.kie.api.task.TaskLifeCycleEventListener;
import org.kie.api.task.model.Task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;


public class MyTaskEventListener implements TaskLifeCycleEventListener
{
	public void afterTaskClaimedEvent(TaskEvent event) {
		System.out.println("task claimed!");
		
		Task task = event.getTask();

		//Long eventId = event.getId();
		//String eventType = event.getType();
		Long taskId = task.getId();
		//Date eventTime = event.getEventTime();
		//String userId = event.getUserId();
		//System.out.println("Event ID: " + eventId);
		//System.out.println("Event Type: " + eventType);
		System.out.println("Task ID: " + taskId);
		//System.out.println("Event Time: " + eventTime);
		//System.out.println("User ID: " + userId);
	}
	
	public void beforeTaskActivatedEvent(TaskEvent event){}
	public void afterTaskReleasedEvent(TaskEvent event){}
	public void beforeTaskClaimedEvent(TaskEvent event){}
	public void beforeTaskStartedEvent(TaskEvent event){}
	public void afterTaskDelegatedEvent(TaskEvent event){}
	public void beforeTaskAddedEvent(TaskEvent event){}
	public void afterTaskExitedEvent(TaskEvent event){}
	public void beforeTaskForwardedEvent(TaskEvent event){}
	public void afterTaskStoppedEvent(TaskEvent event){}
	public void afterTaskSuspendedEvent(TaskEvent event){}
	public void beforeTaskCompletedEvent(TaskEvent event){}
	public void afterTaskFailedEvent(TaskEvent event){}
	public void afterTaskSkippedEvent(TaskEvent event){}
	public void beforeTaskDelegatedEvent(TaskEvent event){}
	public void beforeTaskReleasedEvent(TaskEvent event){}
	public void afterTaskNominatedEvent(TaskEvent event){}
	public void afterTaskAddedEvent(TaskEvent event){}
	public void afterTaskResumedEvent(TaskEvent event){}
	public void beforeTaskExitedEvent(TaskEvent event){}
	public void beforeTaskStoppedEvent(TaskEvent event){}
	public void afterTaskActivatedEvent(TaskEvent event){}
	public void beforeTaskSuspendedEvent(TaskEvent event){}
	public void beforeTaskFailedEvent(TaskEvent event){}
	public void beforeTaskSkippedEvent(TaskEvent event){}
	public void afterTaskStartedEvent(TaskEvent event){}
	public void beforeTaskNominatedEvent(TaskEvent event){}
	public void afterTaskForwardedEvent(TaskEvent event){}
	public void beforeTaskResumedEvent(TaskEvent event){}
	public void afterTaskCompletedEvent(TaskEvent event){}
}
